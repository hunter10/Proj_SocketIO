# Proj_SocketIO

http://meetup.toast.com/posts/112 블로그 참조

https://github.com/nhnent/socket.io-client-unity3d 을 기준으로 작업한것은

클라이언트
- socket.io-client-unity3d-master
- Unity_BestHTTPPro_Test : 에셋 스토어에 있는 BestHTTP Pro 에셋
- Unity_SocketIO_Test2 : 에셋 스토어에 있는 SocketIO for Unity 에셋

서버
접속 테스트용 /NodeJs/TestServer.js
채팅테스트용 /NodeJs/ChatServer.js

접속 테스트할때 클라이언트 이슈
1. socket.io-client-unity3d-master 이지만 바로 다운받아 실행하면 접속 error
    - JSON 파싱에러
    - } 뒤에 붙어있는 문자처리가 안되서 나오는 문제
    - { } 사이값만 읽어들이면 해결
2. Unity_SocketIO_Test2는 기존 애셋스토어에 있는 SocketIO for Unity임 ok
 

2019.2.9. 네이버 메일

hunter10,

We found a potential security vulnerability in a repository for which you have been granted security alert access.

@hunter10  hunter10/Proj_SocketIO   

Known  low severity security vulnerability detected in lodash < 4.17.11 defined in package-lock.json.  
package-lock.json update suggested: lodash ~> 4.17.11.  
Always verify the validity and compatibility of suggestions with your codebase.  
 

