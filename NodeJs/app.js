// Express 기본 모듈 불러오기
var express = require('express'),
    http = require('http'),
    path = require('path');

// Express의 미들웨어 불어오기
var bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    static = require('serve-static'),
    errorHandler = require('errhandler');

// 에러 핸들러 모듈 사용
var expressErrorHandler = require('express-error-handler');

// session 미들웨어 불러오기
var expressSession = require('express-session');

var passport = require('passport');
var flash = require('connect-flash');

// 모듈로 분리한 설정 파일 불러오기
var config = require('./config/config');

var database = require('./database/database');

var route_loader = require('./routes/route_loader');
