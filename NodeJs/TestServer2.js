var io = require('socket.io')({
	transports: ['websocket'],
});

io.attach(4444);

io.on('connection', function(socket){
	socket.on('beep', function(){
    console.log('beep event received');
		socket.emit('boop');
	});
})

console.log('Server started');
