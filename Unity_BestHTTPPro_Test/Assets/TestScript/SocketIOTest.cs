﻿using System;
using System.Collections.Generic;
using UnityEngine;

using BestHTTP.SocketIO;
using hunter10;

public class SocketIOTest : MonoBehaviour
{
    private readonly TimeSpan TYPING_TIMER_LENGTH = TimeSpan.FromMilliseconds(700);

    private enum ChatStates
    {
        Login,
        Chat
    }

    private SocketManager Manager;
    private ChatStates State;
    private string userName = string.Empty;
    private string message = string.Empty;
    private string chatLog = string.Empty;
    private Vector2 scrollPos;
    private bool typing;
    private DateTime lastTypingTime = DateTime.MinValue;
    private List<string> typingUsers = new List<string>();

    // Use this for initialization
    void Start () {
        State = ChatStates.Login;
        SocketOptions options = new SocketOptions();
        options.AutoConnect = false;

        Manager = new SocketManager(new Uri("http://localhost:4444/socket.io/"), options);

        // 받기
        Manager.Socket.On("login", OnLogin);
        Manager.Socket.On("new message", OnNewMessage);

        Manager.Socket.On(SocketIOEventTypes.Error, (socket, packet, args) => Debug.LogError(string.Format("Error: {0}", args[0].ToString())));

        Manager.Open();

        //GUIHelper.ClientArea = new Rect(0, SampleSelector.statisticsHeight + 5, Screen.width, Screen.height - SampleSelector.statisticsHeight - 50);
        UIHelper.ClientArea = new Rect(0, 160 + 5, Screen.width, Screen.height - 160 - 50);
    }

    void OnDestroy()
    {
        // Leaving this sample, close the socket
        Manager.Close();
    }

    void OnGUI()
    {
        switch (State)
        {
            case ChatStates.Login: DrawLoginScreen(); break;
            case ChatStates.Chat: DrawChatScreen(); break;
        }
    }

    void DrawLoginScreen()
    {
        //GUIHelper.DrawArea(GUIHelper.ClientArea, true, () =>
        UIHelper.DrawArea(UIHelper.ClientArea, true, () =>
        {
            GUILayout.BeginVertical();
            GUILayout.FlexibleSpace();

            //GUIHelper.DrawCenteredText("What's your nickname?");
            UIHelper.DrawCenteredText("What's your nickname?");
            userName = GUILayout.TextField(userName);

            if (GUILayout.Button("Join"))
                SetUserName();

            GUILayout.FlexibleSpace();
            GUILayout.EndVertical();
        });
    }

    void DrawChatScreen()
    {
        UIHelper.DrawArea(UIHelper.ClientArea, true, () =>
        {
            GUILayout.BeginVertical();
            scrollPos = GUILayout.BeginScrollView(scrollPos);
            GUILayout.Label(chatLog, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
            GUILayout.EndScrollView();

            string typing = string.Empty;

            if (typingUsers.Count > 0)
            {
                typing += string.Format("{0}", typingUsers[0]);

                for (int i = 1; i < typingUsers.Count; ++i)
                    typing += string.Format(", {0}", typingUsers[i]);

                if (typingUsers.Count == 1)
                    typing += " is typing!";
                else
                    typing += " are typing!";
            }

            GUILayout.Label(typing);

            GUILayout.Label("Type here:");

            GUILayout.BeginHorizontal();
            message = GUILayout.TextField(message);

            if (GUILayout.Button("Send", GUILayout.MaxWidth(100)))
                SendMessage();
            GUILayout.EndHorizontal();

            if (GUI.changed)
                UpdateTyping();

            GUILayout.EndVertical();
        });
    }


    void OnLogin(Socket socket, Packet packet, params object[] args)
    {
        chatLog = "Welcome to Socket.IO Chat — \n";

        addParticipantsMessage(args[0] as Dictionary<string, object>);
    }

    void OnNewMessage(Socket socket, Packet packet, params object[] args)
    {
        addChatMessage(args[0] as Dictionary<string, object>);
    }

    void addParticipantsMessage(Dictionary<string, object> data)
    {
        int numUsers = Convert.ToInt32(data["numUsers"]);

        if (numUsers == 1)
            chatLog += "there's 1 participant\n";
        else
            chatLog += "there are " + numUsers + " participants\n";
    }

    void addChatMessage(Dictionary<string, object> data)
    {
        var username = data["username"] as string;
        var msg = data["message"] as string;

        chatLog += string.Format("Recv : {0}: {1}\n", username, msg);
        
        //string tempMsg = string.Format("{0}: {1}\n", username, msg);
        //Debug.Log(tempMsg);
    }

    void SetUserName()
    {
        if (string.IsNullOrEmpty(userName))
            return;

        State = ChatStates.Chat;

        Manager.Socket.Emit("add user", userName);
    }

    void SendMessage()
    {
        if (string.IsNullOrEmpty(message))
            return;

        Manager.Socket.Emit("new message", message);

        chatLog += string.Format("Send : {0}: {1}\n", userName, message);

        //string tempMsg = string.Format("{0}: {1}\n", userName, message);
        //Debug.Log(tempMsg);

        message = string.Empty;
    }

    void UpdateTyping()
    {
        if (!typing)
        {
            typing = true;
            Manager.Socket.Emit("typing");
        }

        lastTypingTime = DateTime.UtcNow;
    }
}
